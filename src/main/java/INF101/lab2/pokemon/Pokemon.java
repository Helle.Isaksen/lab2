package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    // This is "feltvariabler"
    String name;
    int healthPoints; 
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String name) {

        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
        
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        return (this.healthPoints > 0);
    }

    @Override
    // Regn ut skade
    // kall til damage-metoden på target objektet
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());

        System.out.println(this.name + " attacks " + target.getName());

        target.damage(damageInflicted);

        if (target.isAlive() == false){
            System.out.println(target.getName()+ " is defeated by " + this.name);
        }   
    }

    @Override
    // Subtract damageTaken from currentHP
    // Skal ikke være mulig å gi negativ skade
    // Skriv ut melding
    public void damage(int damageTaken) {
        
        if (damageTaken < 0) {
            System.out.println("you cant do negative damage :(");
            return;
        } else if ((this.healthPoints - damageTaken) > 0) {
            this.healthPoints = this.healthPoints - damageTaken;
        } else {
            this.healthPoints = 0;
        }
        
        System.out.print(this.name + " takes " + damageTaken);
        System.out.print(" damage and is left with ");
        System.out.println(this.healthPoints + "/" + this.maxHealthPoints + " HP");
    }

    @Override
    // convert what happened to string to be printed?? 
    public String toString() {
        String pokemonInfo = "";
        pokemonInfo += this.name + " HP: (" + this.healthPoints + "/" 
        + this.maxHealthPoints + ") STR: " + this.strength;

        return pokemonInfo;
    }

}
